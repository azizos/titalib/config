# Titalib: infrastructure

This repository uses Terraform to setup and configure Kubernetes cluster for [Titalib](https://gitlab.com/azizos/titalib) on Google Cloud (GKE).

In addition, it triggers builds on Continuous Delivery pipeline that updates application deployments on Kubernetes.