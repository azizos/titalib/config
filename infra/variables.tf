variable "project" {
  default = "titalib"
}
variable "region" {
  default = "europe-west4"
}

variable "gke_cluster_name" {
  default = "titalib"
}

variable "gcp_credentials" {
  default = "secrets/gcp_credentials.json"
}
